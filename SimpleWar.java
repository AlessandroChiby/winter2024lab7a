public class SimpleWar {
    public static void main(String[] args) {
        // Initialize deck
        Deck warDeck = new Deck();
        warDeck.shuffle();

        // Initialize player attributes
        int player1Score = 0;
        int player2Score = 0;

        Card player1Card;
        Card player2Card;

        // GameLoop
        while (warDeck.length() > 0) {
            System.out.println("\n***************************************\n");
            // Draw for both players
            player1Card = warDeck.drawTopCard();
            player2Card = warDeck.drawTopCard();
            // Display cards
            System.out.println("Player 1: " + player1Card + "\nPlayer 2: " + player2Card);
            System.out.println("-----------------------------");
            // Compare values
            if (player1Card.calculateScore() > player2Card.calculateScore()) {
                System.out.println("Player 1 wins");
                player1Score++;
            } else {
                System.out.println("Player 2 wins");
                player2Score++;
            }
        }

        System.out.println("\n***************************************\n");
        // If tie
        if (player1Score == player2Score) {
            System.out.println("Both players tie with " + player1Score);
        // If player 1 is higher
        } else if (player1Score > player2Score) {
            System.out.println("Player 1 wins the war with " + player1Score + " points");
            System.out.println("Player 2's points: " + player2Score);
        // If player 2 is higher
        } else {
            System.out.println("Player 2 wins the war with " + player2Score + " points");
            System.out.println("Player 1's points: " + player1Score);
        }
        System.out.println("\n");
    }
}