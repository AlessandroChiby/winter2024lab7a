public class Card {
    private String suit; //hearts, diamonds, clubs, spades
    private int rank; //1-13

    // Constructor
    public Card(String suit, int rank) {
        if (suit.equals("Hearts") || suit.equals("Diamonds") || suit.equals("Clubs") || suit.equals("Spades") && (rank >= 1 && rank <= 13)) {
            this.suit = suit;
            this.rank = rank;
        } else {
            System.out.println("Invalid Suit OR Rank!");
        }
    }

    // Getters
    public String getSuit() {
        return this.suit;
    }
    public int getRank() {
        return this.rank;
    }

    // WarScore
    public double calculateScore() {
        double score = this.rank;
        if (this.suit.equals("Hearts")) {
            score += 0.4;
        } else if (this.suit.equals("Spades")) {
            score += 0.3;
        } else if (this.suit.equals("Diamonds")) {
            score += 0.2;
        } else if (this.suit.equals("Clubs")) {
            score += 0.1;
        }
        return score;
    }

    // toString
    public String toString() {
        String rankName = "";
        if (this.rank == 1) {
            rankName = "Ace";
        } else if (this.rank == 11) {
            rankName = "Jack";
        } else if (this.rank == 12) {
            rankName = "Queen";
        } else if (this.rank == 13) {
            rankName = "King";
        } else {
            rankName = Integer.toString(this.rank);
        }
        return rankName + " of " + suit;
    }
}